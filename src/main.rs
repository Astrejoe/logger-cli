use std::process;
use logger::Command;

fn main() {
    let args: Vec<String> = logger::get_arguments();
    let mut filename: String = String::new();
    let mut error = false;
    if args.len() == 1 {
        filename = String::from("assets/error_files/missing_verb.txt");
        error = true;
    } else if args[1] == logger::FLAGS["help"]["regular"] {
        filename = String::from("assets/help_files/general.txt");
    } else if !logger::check_if_valid_verb(&args[1]) {
        filename = String::from("assets/error_files/invalid_verb.txt");
        error = true;
    } else if args.len() == 2 {
        filename = String::from("assets/error_files/missing_noun.txt");
        error = true;
    } else if args[2] == logger::FLAGS["help"]["regular"] {
        filename = format!("assets/help_files/{}.txt", args[1]);
    } else if args.len() == 4 && args[3] == logger::FLAGS["help"]["regular"] {
        filename = format!("assets/help_files/{}_{}.txt", args[1], args[2]);
    } else if !logger::check_if_valid_noun(&args[1], &args[2]) {
        filename = String::from("assets/error_files/invalid_noun.txt");
        error = true;
    }
    if filename.len() > 0 {
        logger::show_file(filename.as_str(), error);
        if error {
            process::exit(1);
        } else {
            process::exit(0);
        }
    }

    let command = Command::new(&args).unwrap_or_else(|err| {
        eprintln!("Problem parsing arguments: {}", err);
        process::exit(1);
    });

    if command.verb == logger::VERBS["create"] {
        if command.noun == logger::NOUNS["tree"] {
            logger::create_tree(&command.flags);
        } else if command.noun == logger::NOUNS["forest"] {
            logger::create_forest(&command.flags);
        }
    } else if command.verb == logger::VERBS["read"] {
        if command.noun == logger::NOUNS["tree"] {
            logger::read_tree(&command.flags);
        } else if command.noun == logger::NOUNS["forest"] {
            logger::read_forest(&command.flags);
        }
    } else if command.verb == logger::VERBS["update"] {
        if command.noun == logger::NOUNS["tree"] {
            logger::update_tree(&command.flags);
        }
    } else if command.verb == logger::VERBS["delete"] {
        if command.noun == logger::NOUNS["tree"] {
            logger::delete_tree(&command.flags);
        } else if command.noun == logger::NOUNS["forest"] {
            logger::delete_forest(&command.flags);
        }
    } else if command.verb == logger::VERBS["add"] {
        if command.noun == logger::NOUNS["tree"]{
            logger::add_tree(&command.flags);
        }
    } else if command.verb == logger::VERBS["remove"] {
        if command.noun == logger::NOUNS["tree"] {
            logger::remove_tree(&command.flags);
        }
    }
}
