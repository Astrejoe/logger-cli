use std::fs;
use std::error::Error;
use std::env;
use phf::{phf_map, phf_set};
use std::io;
use std::process;

pub static FLAGS: phf::Map<&'static str, phf::Map<&'static str, &'static str>> = phf_map! {
    "help" => phf_map! {
        "short" => "-h",
        "regular" => "--help"
    },
    "height" => phf_map! {
        "short" => "-he",
        "regular" => "--height"
    },
     "circumference" => phf_map! {
        "short" => "-c",
        "regular" => "--circumference"
    },
     "type" => phf_map! {
        "short" => "-ty",
        "regular" => "--type"
    },
     "alive" => phf_map! {
        "short" => "-a",
        "regular" => "--alive"
    },
     "year" => phf_map! {
        "short" => "-y",
        "regular" => "--year"
    },
     "location" => phf_map! {
        "short" => "-l",
        "regular" => "--location"
    },
     "ground" => phf_map! {
        "short" => "-g",
        "regular" => "--ground"
    },
    "tree" => phf_map! {
        "short" => "-t",
        "regular" => "--tree"
    },
    "forest" => phf_map! {
        "short" => "-f",
        "regular" => "--forest"
    },
};

pub static VERBS: phf::Map<&'static str, &'static str> = phf_map! {
    "create" => "create",
    "read" => "read",
    "update" => "update",
    "delete" => "delete",
    "add" => "add",
    "remove" => "remove"
};

pub static NOUNS: phf::Map<&'static str, &'static str> = phf_map! {
    "tree" => "tree",
    "forest" => "forest",
};

pub static VERB_NOUN_COMBINATIONS: phf::Map<&'static str, phf::Set<&'static str>> = phf_map! {
    "create" => phf_set! {
        "tree",
        "forest"
    },
    "read" => phf_set! {
        "tree",
        "forest"
    },
    "update" => phf_set! {
        "tree",
    },
    "delete" => phf_set! {
        "tree",
        "forest"
    },
    "add" => phf_set! {
        "tree",
    },
    "remove" => phf_set! {
        "tree",
    },
};


pub struct Command {
    pub verb: String,
    pub noun: String,
    pub flags: Vec<String>
}

impl Command {
    pub fn new(args: &[String]) -> Result<Command, &'static str> {
        if args.len() < 3 {
            return Err("not enough args");
        }
        let verb = args[1].clone();
        let noun = args[2].clone();
        let mut flags: Vec<String> = Vec::new();
        if args.len() > 3 {
            flags.resize(args[3..].len(), String::new());
            flags.clone_from_slice(&args[3..])
        }

        Ok(Command { verb, noun, flags })
    }

    pub fn print(&self) {
        println!("command:");
        println!("verb: {}", self.verb);
        println!("noun: {}", self.noun);
        println!("flags: {:?}", self.flags);
    }
}

pub struct Tree {
    pub height: f64,
    pub circumference: f64,
    pub tree_type: String,
    pub alive: bool,
    pub year: f64,
    pub location: String,
    pub ground: String,
    pub id: String,
}

impl Tree {
    pub fn print(&self) {
        if self.height != 0.0 {
            println!("Height: {}cm", self.height);
        }
        if self.circumference != 0.0 {
            println!("Circumference: {}cm", self.circumference);
        }
        if self.tree_type != "" {
            println!("Tree type: {}", self.tree_type);
        }
        println!("Alive: {}", self.alive);
        if self.year != 0.0 {
            println!("Plant year: {}", self.year);
        }
        if self.ground != "" {
            println!("Ground type: {}", self.ground);
        }
        if self.location != "" {
            println!("Location: {}", self.location);
        }
        println!("ID: {}", self.id);
    }

    pub fn get (&self, property: &str) -> String {
        match property {
            "height" => format!("{}cm", self.height),
            "circumference" => format!("{}cm", self.circumference),
            "tree_type" => self.tree_type.clone(),
            "alive" => format!("{}", self.alive),
            "year" => format!("{}", self.year),
            "location" => self.location.clone(),
            "ground" => self.ground.clone(),
            "id" => self.id.clone(),
            _ => {
                eprintln!("{} is not a property of tree", property);
                process::exit(1);
            }
        }
    }
}

pub struct Forest {
    pub id: String,
    pub trees: Vec<Tree>
}

impl Forest {
    pub fn print(&self, properties: &Vec<String>) {
        print!("{0: <15}", "tree ID");
        for property in properties {
            print!("{0: <15}", property);
        }
        println!();
        for tree in &self.trees {
            print!("{0: <15}", tree.id);
            for property in properties {
                print!("{0: <15}", tree.get(property.as_str()));
            }
            println!();
        }
    }
}

pub fn show_file(filename: &str, error: bool) -> Result<(), Box<dyn Error>>{
    let content = fs::read_to_string(filename)?;
    if error {
        println!("{}", content);
    } else {
        eprintln!("{}", content);
    }
    Ok(())
}

pub fn get_arguments() -> Vec<String> {
    convert_short_flags_to_long_flags(env::args().collect())
}

pub fn convert_short_flags_to_long_flags (args: Vec<String>) -> Vec<String> {
    let mut result: Vec<String> = Vec::new();
    for arg in args {
        result.push(convert_short_flag_to_long_flag(arg));
    }
    return result
}

pub fn convert_short_flag_to_long_flag (flag: String) -> String {
    for possible_flag in FLAGS.values() {
        if flag == possible_flag["short"] {
            return String::from(possible_flag["regular"]);
        } else if flag == possible_flag["regular"] {
            break;
        }
    }
    return flag;
}

pub fn check_for_help_flag (args: Vec<String>) -> bool {
    for arg in args {
        if arg == "--help" {
            return true
        }
    }
    return false
}

pub fn check_if_valid_verb (verb: &str) -> bool {
    return VERBS.contains_key(verb);
}

pub fn check_if_valid_noun (verb: &str, noun: &str) -> bool {
    return VERB_NOUN_COMBINATIONS[verb].contains(noun);
}

pub fn ask_number_input (default_value: f64, request_text: &'static str, error_text: &'static str) -> f64 {
    let default_value_string = format!("{}", default_value);
    return string_to_number(ask_string_input(default_value_string, request_text) , error_text);
}

pub fn ask_bool_input (default_value: bool, request_text: &'static str, error_text: &'static str) -> bool {
    let default_value_string= bool_to_string(default_value);
    let string_input = ask_string_input(default_value_string, request_text);
    return string_to_bool(string_input, error_text);

}

pub fn bool_to_string (boolean: bool) -> String {
    return if boolean {
        String::from("y")
    } else {
        String::from("n")
    }
}

pub fn string_to_bool (string: String, error_text: &str) -> bool {
    return match string.as_str() {
        "y" => true,
        "Y" => true,
        "yes" => true,
        "Yes" => true,
        "true" => true,
        "True" => true,
        "n" => false,
        "N" => false,
        "no" => false,
        "No" => false,
        "false" => false,
        "False" => false,
        _ => {
            eprintln!("ERROR: {}", error_text);
            process::exit(1);
        }
    }
}

pub fn ask_string_input (default_value: String, request_text: &'static str) -> String {
    let mut input  = String::new();
    println!("{} Press CTRL-C to exit.", request_text);
    io::stdin().read_line(&mut input);
    input.trim();
    input.pop(); // remove trailing \n
    if !input.is_empty() {
        return String::from(input);
    } else {
        return default_value
    }
}

pub fn string_to_number (string: String, error_text: &str) -> f64 {
    return match string.parse() {
        Ok(num) => num,
        Err(_) => {
            eprintln!("ERROR: {}", error_text);
            process::exit(1);
        },
    };
}

pub fn get_number_value_flag (default_value: f64, flags: &Vec<String>, flag: &'static str) -> f64 {
    let default_value_string = format!("{}", default_value);
    let error_text = format!("The value of flag {} must be a number", flag);
    return string_to_number(get_string_value_flag(default_value_string, flags, flag), error_text.as_str());
}

pub fn get_bool_value_flag (default_value: bool, flags: &Vec<String>, flag: &str) -> bool {
    let default_value_string= bool_to_string(default_value);
    let error_text = format!("The value of flag {} must be true or false", flag);
    return string_to_bool(get_string_value_flag(default_value_string, flags, flag), error_text.as_str());
}

pub fn get_string_value_flag (default_value: String, flags: &Vec<String>, flag: &str) -> String {
    let flag_string = String::from(flag);
    if !flags.contains(&flag_string) {
        return default_value;
    }
    let index_flag = flags.iter().position(|r| r == flag).unwrap();
    if flags.len() - 1 == index_flag || &flags.get(index_flag + 1).unwrap()[0..2] == "--" {
        eprintln!("ERROR: no value provided for flag {}", flag);
        process::exit(1);
    }
    return flags.get(index_flag + 1).unwrap().clone();
}

pub fn create_tree (flags: &Vec<String>) {
    let mut height = 0.0;
    let mut circumference = 0.0;
    let mut tree_type = String::new();
    let mut alive = true;
    let mut year = 0.0;
    let mut location = String::new();
    let mut ground = String::new();
    let mut id = String::from("2BL3dHo9B2H43");
    if flags.len() == 0 {
        height = ask_number_input(0.0, "Enter the HEIGHT of the tree in cm, e.g. 108. Press ENTER to leave empty.", "Height must be a number.");
        circumference = ask_number_input(0.0, "Enter the CIRCUMFERENCE of the tree in cm, e.g. 26. Press ENTER to leave empty.", "Circumference must be a number.");
        tree_type = ask_string_input(String::new(), "Enter the TYPE of tree, e.g. oak. Press ENTER to leave empty.");
        alive = ask_bool_input(true, "Enter whether the tree is alive (y/n). Default is yes.", "Alive must be y or n.");
        year = ask_number_input(0.0, "Enter the YEAR that the tree was planted, e.g. 2020. Press ENTER to leave empty.", "Year must be a number.");
        location = ask_string_input(String::new(), "Enter the LOCATION of tree, e.g. 53.4N12.9E. Press ENTER to leave empty.");
        ground = ask_string_input(String::new(), "Enter the TYPE of ground, e.g. lime. Press ENTER to leave empty.");
        id = ask_string_input(String::from("2BL3dHo9B2H43"), "Enter the ID for the tree. Press ENTER to automatically generate one.");
    } else {
        height = get_number_value_flag(0.0, flags, FLAGS["height"]["regular"]);
        circumference = get_number_value_flag(0.0, flags, FLAGS["circumference"]["regular"]);
        tree_type = get_string_value_flag(String::new(), flags, FLAGS["type"]["regular"]);
        alive = get_bool_value_flag(true, flags, FLAGS["alive"]["regular"]);
        year = get_number_value_flag(0.0, flags, FLAGS["year"]["regular"]);
        location = get_string_value_flag(String::new(), flags, FLAGS["location"]["regular"]);
        ground = get_string_value_flag(String::new(), flags, FLAGS["ground"]["regular"]);
        id = get_string_value_flag(String::from("2BL3dHo9B2H43"), flags, FLAGS["tree"]["regular"]);
    }
    let tree = Tree { height, circumference, tree_type, alive, year, location, ground, id };
    println!("Successfully created tree with the following properties:");
    tree.print();
}

pub fn get_example_tree() -> Tree {
    return Tree { height: 84.5, circumference: 23.3, tree_type: String::from("spruce"), alive: true, year: 2012.0, location: String::from("31.2N95.3E"), ground: String::from("sand"), id: String::from("Hd2Dn42") };
}

pub fn get_example_forest() -> Forest {
    let tree_1 = get_example_tree();
    let tree_2 = Tree { height: 67.3, circumference: 15.9, tree_type: String::from("spruce"), alive: true, year: 2011.0, location: String::from("31.1N95.4E"), ground: String::from("sand"), id: String::from("8JBkH32") };
    let tree_3 = Tree { height: 98.5, circumference: 32.5, tree_type: String::from("oak"), alive: false, year: 2009.0, location: String::from("30.9N95.6E"), ground: String::from("lime"), id: String::from("Kb32J4H1") };
    let tree_4 = Tree { height: 56.9, circumference: 20.1, tree_type: String::from("willow"), alive: true, year: 2017.0, location: String::from("31.2N95.2E"), ground: String::from("lime"), id: String::from("9KbK43j9") };
    let tree_5 = Tree { height: 26.4, circumference: 9.2, tree_type: String::from("oak"), alive: false, year: 2019.0, location: String::from("31.8N95.4E"), ground: String::from("sand"), id: String::from("Jok43Hbd") };
    let trees = vec!(tree_1, tree_2, tree_3, tree_4, tree_5);
    return Forest { id: String::from("5sKb82Bd"), trees};
}

pub fn read_tree(flags: &Vec<String>) {
    let example_tree = get_example_tree();
    let mut id = String::new();
    if !flags.contains(&FLAGS["tree"]["regular"].to_string()){
        // interactive
        id = ask_string_input(String::new(), "Enter the ID of the tree to read and press ENTER.");
        if id.is_empty() {
            eprintln!("ERROR: No tree ID provided");
            process::exit(1);
        }
        if id != example_tree.id {
            println!("Could not find tree with ID {}.", id);
            process::exit(0);
        }
        println!("Properties of tree with ID {}:", id);
        example_tree.print();
    } else {
        // non-interactive
        id = get_string_value_flag(String::new(), flags, FLAGS["tree"]["regular"]);
        if id != example_tree.id {
            println!("Could not find tree with ID {}.", id);
            process::exit(0);
        }
        println!("Properties of tree with ID {}:", id);
        if flags.len() <= 2 {
            example_tree.print();
            process::exit(0);
        }
        if flags.contains(&String::from(FLAGS["height"]["regular"])) {
            println!("Height: {}cm", example_tree.height);
        }
        if flags.contains(&String::from(FLAGS["circumference"]["regular"])) {
            println!("Circumference: {}cm", example_tree.circumference);
        }
        if flags.contains(&String::from(FLAGS["type"]["regular"])) {
            println!("Tree type: {}", example_tree.tree_type);
        }
        if flags.contains(&String::from(FLAGS["alive"]["regular"])) {
            println!("Alive: {}", example_tree.alive);
        }
        if flags.contains(&String::from(FLAGS["year"]["regular"])) {
            println!("Plant year: {}", example_tree.year);
        }
        if flags.contains(&String::from(FLAGS["ground"]["regular"])) {
            println!("Ground type: {}", example_tree.ground);
        }
        if flags.contains(&String::from(FLAGS["location"]["regular"])) {
            println!("Location: {}", example_tree.location);
        }
    }
}

pub fn update_tree(flags: &Vec<String>) {
    let example_tree = get_example_tree();
    let mut id = String::new();
    let mut height = 0.0;
    let mut circumference = 0.0;
    let mut tree_type = String::new();
    let mut alive = true;
    let mut year = 0.0;
    let mut location = String::new();
    let mut ground = String::new();
    if !flags.contains(&FLAGS["tree"]["regular"].to_string()) {
        // interactive
        id = ask_string_input(String::new(), "Enter the ID of the tree to update and press ENTER.");
        if id.is_empty() {
            eprintln!("ERROR: No tree ID provided");
            process::exit(1);
        }
        if id != example_tree.id {
            println!("Could not find tree with ID {}.", id);
            process::exit(0);
        }
        height = ask_number_input(0.0, "Enter the HEIGHT of the tree in cm. Currently: 84.5. Press ENTER to leave unchanged.", "Height must be a number.");
        circumference = ask_number_input(0.0, "Enter the CIRCUMFERENCE of the tree in cm. Currently: 23.3. Press ENTER to leave unchanged.", "Circumference must be a number.");
        tree_type = ask_string_input(String::new(), "Enter the TYPE of tree. Currently: spruce. Press ENTER to leave unchanged.");
        alive = ask_bool_input(true, "Enter whether the tree is alive (y/n). Currently: true. Press ENTER to leave unchanged.", "Alive must be y or n.");
        year = ask_number_input(0.0, "Enter the YEAR that the tree was planted. Currently: 2012. Press ENTER to leave unchanged.", "Year must be a number.");
        location = ask_string_input(String::new(), "Enter the LOCATION of tree. Currently: 31.2N95.3E. Press ENTER to leave unchanged.");
        ground = ask_string_input(String::new(), "Enter the TYPE of ground. Currently: sand. Press ENTER to leave unchanged.");
    } else {
        id = get_string_value_flag(String::new(), flags, FLAGS["tree"]["regular"]);
        if id != example_tree.id {
            println!("Could not find tree with ID {}.", id);
            process::exit(0);
        }
        height = get_number_value_flag(0.0, flags, FLAGS["height"]["regular"]);
        circumference = get_number_value_flag(0.0, flags, FLAGS["circumference"]["regular"]);
        tree_type = get_string_value_flag(String::new(), flags, FLAGS["type"]["regular"]);
        alive = get_bool_value_flag(true, flags, FLAGS["alive"]["regular"]);
        year = get_number_value_flag(0.0, flags, FLAGS["year"]["regular"]);
        location = get_string_value_flag(String::new(), flags, FLAGS["location"]["regular"]);
        ground = get_string_value_flag(String::new(), flags, FLAGS["ground"]["regular"]);
    }
    let tree = Tree { height, circumference, tree_type, alive, year, location, ground, id };
    println!("Successfully updated the tree with ID {} with the following properties:", example_tree.id);
    tree.print();
}

pub fn delete_tree(flags: &Vec<String>) {
    let example_tree = get_example_tree();
    let mut id = String::new();
    if !flags.contains(&FLAGS["tree"]["regular"].to_string()) {
        // interactive
        id = ask_string_input(String::new(), "Enter the ID of the tree to delete and press ENTER.");
        if id.is_empty() {
            eprintln!("ERROR: No tree ID provided");
            process::exit(1);
        }
        if id != example_tree.id {
            println!("Could not find tree with ID {}.", id);
            process::exit(0);
        }
    } else {
        id = get_string_value_flag(String::new(), flags, FLAGS["tree"]["regular"]);
        if id != example_tree.id {
            println!("Could not find tree with ID {}.", id);
            process::exit(0);
        }
    }
    println!("Successfully deleted tree with ID {}.", id);
}

pub fn create_forest(flags: &Vec<String>) {
    let mut tree_ids: Vec<String> = Vec::new();
    let mut input = String::new();
    let mut id = String::from("3Bl0Hbd1");
    if flags.len() == 0 {
        // interactive mode
        id = ask_string_input(id.clone(), "Enter an ID for the forest and press ENTER. Leave blank to automatically generate an ID.");
        while  {
            input = ask_string_input(String::new(), "Enter the ID of a tree and press ENTER to add that tree to the forest. Leave blank to finish.");
            if !input.is_empty() {
                tree_ids.push(input.clone());
            }
            !input.is_empty()
        }{}
    } else {
        // non-interactive
        id = get_string_value_flag(id.clone(), flags, FLAGS["forest"]["regular"]);
        let tree_flag_string = String::from(FLAGS["tree"]["regular"]);
        if flags.contains(&tree_flag_string) {
            let mut next_value = String::new();
            let mut index = flags.iter().position(|r| r == FLAGS["tree"]["regular"]).unwrap();
            while {
                next_value.clear();
                index += 1;
                if flags.len() == index || &flags.get(index).unwrap()[0..2] == "--" {
                    false
                } else {
                    tree_ids.push(String::from(flags.get(index).unwrap()));
                    true
                }
            } {}
        }
    }
    println!("Successfully created forest with ID {}", id);
    if !tree_ids.is_empty() {
        println!("trees added to forest: {:?}", tree_ids);
    }
}

pub fn read_forest(flags: &Vec<String>) {
    let example_forest = get_example_forest();
    let mut id = String::new();
    let all_properties = vec!(String::from("height"), String::from("circumference"), String::from("tree_type"), String::from("alive"), String::from("year"), String::from("location"), String::from("ground"));
    let mut properties: Vec<String> = Vec::new();
    if !flags.contains(&FLAGS["forest"]["regular"].to_string()) {
        // interactive
        id = ask_string_input(id.clone(), "Enter an ID for the forest and press ENTER.");
        if id.is_empty() {
            eprintln!("ERROR: No forest ID provided");
            process::exit(1);
        }
        if id != example_forest.id {
            println!("Could not find forest with ID {}.", id);
            process::exit(0);
        }
        example_forest.print(&all_properties);
    } else {
        id = get_string_value_flag(String::new(), flags, FLAGS["forest"]["regular"]);
        if id != example_forest.id {
            println!("Could not find forest with ID {}.", id);
            process::exit(0);
        }
        if flags.contains(&FLAGS["height"]["regular"].to_string()) {
            properties.push("height".to_string());
        }
        if flags.contains(&FLAGS["circumference"]["regular"].to_string()) {
            properties.push("circumference".to_string());
        }
        if flags.contains(&FLAGS["type"]["regular"].to_string()) {
            properties.push("tree_type".to_string());
        }
        if flags.contains(&FLAGS["alive"]["regular"].to_string()) {
            properties.push("alive".to_string());
        }
        if flags.contains(&FLAGS["year"]["regular"].to_string()) {
            properties.push("year".to_string());
        }
        if flags.contains(&FLAGS["location"]["regular"].to_string()) {
            properties.push("location".to_string());
        }
        if flags.contains(&FLAGS["ground"]["regular"].to_string()) {
            properties.push("ground".to_string());
        }
        if properties.is_empty() {
            example_forest.print(&all_properties);
        } else {
            example_forest.print(&properties);
        }
    }
}

pub fn delete_forest(flags: &Vec<String>) {
    let example_forest = get_example_forest();
    let mut id = String::new();
    if !flags.contains(&FLAGS["forest"]["regular"].to_string()) {
        id = ask_string_input(id.clone(), "Enter the ID of the forest to delete and press ENTER.");
        if id.is_empty() {
            eprintln!("ERROR: No forest ID provided");
            process::exit(1);
        }
        if id != example_forest.id {
            println!("Could not find forest with ID {}.", id);
            process::exit(0);
        }
    } else {
        id = get_string_value_flag(String::new(), flags, FLAGS["forest"]["regular"]);
        if id != example_forest.id {
            println!("Could not find forest with ID {}.", id);
            process::exit(0);
        }
    }
    println!("Successfully deleted forest with ID {}.", id);
}

pub fn add_tree(flags: &Vec<String>) {
    let example_forest = get_example_forest();
    let mut tree_ids: Vec<String> = Vec::new();
    let mut input = String::new();
    let mut id = String::new();
    if !flags.contains(&FLAGS["forest"]["regular"].to_string()) || !flags.contains(&FLAGS["tree"]["regular"].to_string()){
        // interactive mode
        id = ask_string_input(String::new(), "Enter the ID of the forest to add (a) tree(s) to and press ENTER.");
        if id.is_empty() {
            eprintln!("ERROR: No forest ID provided");
            process::exit(1);
        }
        if id != example_forest.id {
            println!("Could not find forest with ID {}.", id);
            process::exit(0);
        }
        while {
            input = ask_string_input(String::new(), "Enter the ID of a tree and press ENTER to add that tree to the forest. Leave blank to finish.");
            if !input.is_empty() {
                tree_ids.push(input.clone());
            }
            !input.is_empty()
        }{}
    } else {
        // non-interactive
        id = get_string_value_flag(id.clone(), flags, FLAGS["forest"]["regular"]);
        if id != example_forest.id {
            println!("Could not find forest with ID {}.", id);
            process::exit(0);
        }
        let tree_flag_string = String::from(FLAGS["tree"]["regular"]);
        let mut next_value = String::new();
        let mut index = flags.iter().position(|r| r == FLAGS["tree"]["regular"]).unwrap();
        while {
            next_value.clear();
            index += 1;
            if flags.len() == index || &flags.get(index).unwrap()[0..2] == "--" {
                false
            } else {
                tree_ids.push(String::from(flags.get(index).unwrap()));
                true
            }
        } {}
    }
    if tree_ids.is_empty() {
        println!("No trees added to forest with ID {}.", id);
    } else {
        println!("Successfully added the following tree to forest with ID {}: {:?}", id, tree_ids);
    }
}

pub fn remove_tree(flags: &Vec<String>) {
    let example_forest = get_example_forest();
    let mut tree_ids: Vec<String> = Vec::new();
    let mut input = String::new();
    let mut id = String::new();
    if !flags.contains(&FLAGS["forest"]["regular"].to_string()) || !flags.contains(&FLAGS["tree"]["regular"].to_string()){
        // interactive mode
        id = ask_string_input(String::new(), "Enter the ID of the forest to remove (a) tree(s) from and press ENTER.");
        if id.is_empty() {
            eprintln!("ERROR: No forest ID provided");
            process::exit(1);
        }
        if id != example_forest.id {
            println!("Could not find forest with ID {}.", id);
            process::exit(0);
        }
        while {
            input = ask_string_input(String::new(), "Enter the ID of a tree and press ENTER to remove that tree to the forest. Leave blank to finish.");
            if !input.is_empty() {
                tree_ids.push(input.clone());
            }
            !input.is_empty()
        }{}
    } else {
        // non-interactive
        id = get_string_value_flag(id.clone(), flags, FLAGS["forest"]["regular"]);
        if id != example_forest.id {
            println!("Could not find forest with ID {}.", id);
            process::exit(0);
        }
        let tree_flag_string = String::from(FLAGS["tree"]["regular"]);
        let mut next_value = String::new();
        let mut index = flags.iter().position(|r| r == FLAGS["tree"]["regular"]).unwrap();
        while {
            next_value.clear();
            index += 1;
            if flags.len() == index || &flags.get(index).unwrap()[0..2] == "--" {
                false
            } else {
                tree_ids.push(String::from(flags.get(index).unwrap()));
                true
            }
        } {}
    }
    if tree_ids.is_empty() {
        println!("No trees removed from forest with ID {}.", id);
    } else {
        println!("Successfully removed the following tree to forest with ID {}: {:?}", id, tree_ids);
    }
}
