# Logger

## Build and use
### Install Rust
The application is written in Rust. If you are on Linux, you can install Rust through the following command:
```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
``` 
Click [here](https://www.rust-lang.org/tools/install) for more instructions.

### build the application
Run the following command from the root dir to build the application:
```
cargo build
```
The built application is in target/debug. Go to ths folder with the following command:
```
cd ./target/debug/
```

### Use the application
If you are in the ./target/debug folder, you can use the application as follows:
```
./logger <VERB> <NOUN> [FLAGS]
```
Run 
```
./logger --help
```
for more information on how to use logger.